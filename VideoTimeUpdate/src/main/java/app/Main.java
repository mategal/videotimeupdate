package app;

import com.sun.jna.NativeLibrary;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{


        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("sample.fxml"));
        Image icon = new Image(getClass().getClassLoader().getResourceAsStream("image/cropped-onion-512.png"));
        primaryStage.getIcons().add(icon);
        primaryStage.setTitle("Cebulotron 2077");
        primaryStage.setScene(new Scene(root, 600, 275));
        primaryStage.setResizable(false);
        primaryStage.show();



    }


    public static void main(String[] args) {
        launch(args);
    }
}
