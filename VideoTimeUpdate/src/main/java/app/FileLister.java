package app;

import com.abercap.mediainfo.api.MediaInfo;
import javafx.concurrent.Task;

import java.io.*;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;


public class FileLister extends Task<Void> {



    public FileLister() {
    }

    @Override
    protected Void call() throws Exception {

        ExcelWitexPattern excelWitexPattern = new ExcelWitexPattern();
        MediaInfo mediaInfo = new MediaInfo();
        FileOutputStream fileOutputStream;

        try {
            fileOutputStream = new FileOutputStream(Settings.getOutputScannedPath());




        int nd = 1;

        for (File directory : Settings.getListOfScannedPaths()) {

            double count = 0;
            double files = directory.listFiles().length;
            updateMessage(directory.getAbsolutePath());


            for (File file : directory.listFiles()) {
                Counter.setFilesCount(Counter.getFilesCount() + 1);
                count++;
                if (file.isFile()) {

                    mediaInfo.open(file);


                    String durationTemp = mediaInfo.get(MediaInfo.StreamKind.Video, 0, "Duration", MediaInfo.InfoKind.Text, MediaInfo.InfoKind.Name);
                    String durationAudioTemp = mediaInfo.get(MediaInfo.StreamKind.Audio, 0, "Duration", MediaInfo.InfoKind.Text, MediaInfo.InfoKind.Name);
                    String frameRateTemp = mediaInfo.get(MediaInfo.StreamKind.Video, 0, "FrameRate", MediaInfo.InfoKind.Text, MediaInfo.InfoKind.Name);



                    if(!durationTemp.equals("") && !frameRateTemp.equals("")){
                        long duration = (Long.parseLong(durationTemp) >= Long.parseLong(durationAudioTemp)? Long.parseLong(durationTemp) : Long.parseLong(durationAudioTemp));
                        float frameRate = Float.parseFloat(frameRateTemp);

                    String dataToWitexUpdate = (nd +","+","+ framesToTimeConvert(duration, frameRate) +","+","+","+file.getName().substring(0,file.getName().length()-4)+","+","+","+",");
                    excelWitexPattern.addRowWithData(dataToWitexUpdate);
                    System.out.println(nd +","+","+ framesToTimeConvert(duration, frameRate) +","+","+","+file.getName().substring(0,file.getName().length()-4)+","+","+","+",");
                    nd++;
                    }

                }
                updateProgress(count, files);
            }
            Counter.setFilesCount(0);

        }

        excelWitexPattern.saveExcelFile(fileOutputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }


    private String framesToTimeConvert(long duration, float frameRate){



        //6914320/1000*25 mod 25 <-- frames

        long hours = TimeUnit.MILLISECONDS.toHours(duration);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(duration) - TimeUnit.HOURS.toMinutes(hours);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(duration) - TimeUnit.MINUTES.toSeconds(minutes) - TimeUnit.HOURS.toSeconds(hours);
        long frames = (long)((duration*(frameRate*0.001F))%frameRate);
        String time = String.format("%02d:%02d:%02d:%02d",hours,minutes,seconds,frames);

        return time;



    }

}
