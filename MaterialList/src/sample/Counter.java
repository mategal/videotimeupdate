package sample;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

public class Counter {
    static double directoriesNumber = Settings.getListOfScannedPaths().size();
    static double directoriesCounted = 0;

    static double filesNumber;
    static double filesCount = 0;

    static double filesProgressBarUpdate =0;
    static DoubleProperty filesProgressBarProperty = new SimpleDoubleProperty(0);

    static double directoriesProgressBarUpdate = 0;

    public static double getDirectoriesNumber() {
        return directoriesNumber;
    }

    public static void setDirectoriesNumber(double directoriesNumber) {
        Counter.directoriesNumber = directoriesNumber;
    }

    public static double getDirectoriesCounted() {
        return directoriesCounted;
    }

    public static void setDirectoriesCounted(double directoriesCounted) {
        Counter.directoriesCounted = directoriesCounted;
        Counter.directoriesProgressBarUpdate = (directoriesCounted/directoriesNumber);
    }

    public static double getFilesNumber() {
        return filesNumber;
    }

    public static void setFilesNumber(double filesNumber) {
        Counter.filesNumber = filesNumber;
    }

    public static double getFilesCount() {
        return filesCount;
    }

    public static void setFilesCount(double filesCount) {
        Counter.filesCount = filesCount;
        Counter.filesProgressBarUpdate = (filesCount/filesNumber);
        filesProgressBarProperty.set(filesProgressBarUpdate);

    }

    public static double getFilesProgressBarUpdate() {
        return filesProgressBarUpdate;
    }

    public static double getDirectoriesProgressBarUpdate() {
        return directoriesProgressBarUpdate;
    }

    public static double getFilesProgressBarProperty() {
        return filesProgressBarProperty.get();
    }

    public static DoubleProperty filesProgressBarPropertyProperty() {
        return filesProgressBarProperty;
    }
}
