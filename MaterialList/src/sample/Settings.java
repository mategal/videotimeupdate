package sample;


import java.io.*;
import java.util.*;

public class Settings {

    static HashMap<String, File> listOfScannedPathsHashMap = new HashMap<>();

        static String outputPath = null;
    static Properties prop = new Properties();
    static InputStream input = null;
    static OutputStream output = null;

    public static ArrayList<File> getListOfScannedPaths() {
        ArrayList<File> fileArrayList;
        Collection<File> values = listOfScannedPathsHashMap.values();
        fileArrayList = new ArrayList<>(values);

        return fileArrayList;
    }

    public static void addListOfScannedPaths(File scannedPath) {
        listOfScannedPathsHashMap.put(scannedPath.getAbsolutePath(), scannedPath);
        saveSettings();
    }

    public static void removeScannedPath(String path){
        listOfScannedPathsHashMap.remove(path);
        saveSettings();
    }

    public static void loadSettings() {
        try {
            input = new FileInputStream(System.getProperty("user.home") + "//MaterialListConfig.properties");
            prop.load(input);
            if(prop.getProperty("scanned_paths") != null) {
                List<String> arrayOfScannedPaths = Arrays.asList(prop.getProperty("scanned_paths").split(";"));
                for (String scannedPath : arrayOfScannedPaths) {
                    listOfScannedPathsHashMap.put(scannedPath,new File(scannedPath));
                }
            }
            if(prop.getProperty("output_path")!= null){
                outputPath =  prop.getProperty("output_path");
            }
        } catch (FileNotFoundException e) {
            PrintWriter writer = null;
            try {
                writer = new PrintWriter(System.getProperty("user.home") + "//MaterialListConfig.properties");
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            }
            writer.close();
            saveSettings();
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void saveSettings() {
        try {
            output = new FileOutputStream(System.getProperty("user.home") + "//MaterialListConfig.properties");

            StringBuffer scannedPathsProperty = new StringBuffer();

            ArrayList<File> listOfScannedPaths;

            Collection<File> values = listOfScannedPathsHashMap.values();
            listOfScannedPaths = new ArrayList<>(values);



            for(File path: listOfScannedPaths){
                scannedPathsProperty.append(path.getAbsolutePath()+";");
            }

            prop.setProperty("scanned_paths", String.valueOf(scannedPathsProperty));

            if(outputPath != null) {
                prop.setProperty("output_path", outputPath);
            }
            prop.store(output,null);
        } catch (FileNotFoundException e) {
            PrintWriter writer = null;
            try {
                writer = new PrintWriter(System.getProperty("user.home") + "//MaterialListConfig.properties");
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            }
            writer.close();
            saveSettings();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public static void setOutputScannedPath(String outputScannedPath){
            outputPath = outputScannedPath;
        PrintWriter writer = null;

        try {
            writer = new PrintWriter(outputPath);
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        }
        writer.close();

        saveSettings();
    }

    public static void resetConfig(){
        try {
            output = new FileOutputStream(System.getProperty("user.home") + "//MaterialListConfig.properties");
            listOfScannedPathsHashMap = new HashMap<>();
            outputPath = null;
            prop.remove("scanned_paths");
            prop.remove("output_path");
            prop.store(output,null);
        } catch (FileNotFoundException e) {
            PrintWriter writer = null;
            try {
                writer = new PrintWriter(System.getProperty("user.home") + "//MaterialListConfig.properties");
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            }
            writer.close();
            saveSettings();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static String getOutputScannedPath() {
        return outputPath;
    }
}
