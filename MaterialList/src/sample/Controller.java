package sample;

import java.beans.EventHandler;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import com.sun.tools.classfile.ConstantPool;
import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import sun.jvmstat.perfdata.monitor.CountedTimerTask;


public class Controller {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private ListView listOfPaths;

    @FXML
    private Menu about;

    @FXML
    private Label currentDirectory;

    @FXML
    private ProgressBar filesProgressBar;

    @FXML
    private Button addPathButton;

    @FXML
    private Button removePathButton;

    @FXML
    private Button scanAndSaveButton;

    @FXML
    void resetConfigClick(ActionEvent event) {
        Settings.resetConfig();
        listOfPaths.setItems(null);
        scanAndSaveButton.setDisable(true);
        removePathButton.setDisable(true);
    }

    @FXML
    void AddPathClick(MouseEvent event) {

        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Wybierz katalog do skanowania:");
        File directoryPath = directoryChooser.showDialog(addPathButton.getScene().getWindow());
        Settings.addListOfScannedPaths(directoryPath);
        listOfPaths.setItems(fillListViewWithPaths());
        scanAndSaveButton.setDisable(false);
        removePathButton.setDisable(false);
    }

    @FXML
    void removePathClick(MouseEvent event) {
        if (listOfPaths.getSelectionModel().getSelectedItem() != null) {
            Settings.removeScannedPath(listOfPaths.getSelectionModel().getSelectedItem().toString());
            listOfPaths.setItems(fillListViewWithPaths());
        }
        if(!listOfPaths.getItems().isEmpty()){
            scanAndSaveButton.setDisable(false);
            removePathButton.setDisable(false);
        }else{
            scanAndSaveButton.setDisable(true);
            removePathButton.setDisable(true);
        }
    }

    @FXML
    void scanAndSaveClick(MouseEvent event) {

        scanAndSaveButton.setDisable(true);
        addPathButton.setDisable(true);
        removePathButton.setDisable(true);
        File output;

        if (Settings.getOutputScannedPath() == null) {
            DirectoryChooser directoryChooser = new DirectoryChooser();
            directoryChooser.setTitle("Wybierz miejsce zapisu listy plików:");
            output = directoryChooser.showDialog(scanAndSaveButton.getScene().getWindow());

            TextInputDialog dialog = new TextInputDialog("ListOfFiles");
            dialog.setTitle("Nazwa pliku");
            dialog.setHeaderText("");
            dialog.setContentText("Wprowadź nazwę pliku(bez rozszerzenia):");

            // Traditional way to get the response value.
            Optional<String> result = dialog.showAndWait();

            if (result.isPresent()) {
                Settings.setOutputScannedPath(output.getAbsolutePath() + "//" + result.get() + ".txt");
            } else {
                Settings.setOutputScannedPath(output.getAbsolutePath() + "//" + dialog.getDefaultValue() + ".txt");
            }

            Settings.saveSettings();
            output = new File(Settings.getOutputScannedPath());
        } else {
            output = new File(Settings.getOutputScannedPath());
        }




        Task<Void> fileLister = new FileLister();
        File finalOutput = output;
        fileLister.setOnSucceeded(e -> {

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("INFO");
            alert.setHeaderText("");
            alert.setContentText("Foldery przeskanowane i zapisane pod ścieżką " + finalOutput.getAbsolutePath());
            alert.showAndWait();

            scanAndSaveButton.setDisable(false);
            addPathButton.setDisable(false);
            removePathButton.setDisable(false);
        });







        filesProgressBar.progressProperty().unbind();
        filesProgressBar.progressProperty().bind(fileLister.progressProperty());
        currentDirectory.textProperty().bind(fileLister.messageProperty());
        Thread thread = new Thread(fileLister);
        thread.setDaemon(true);
        thread.start();


    }

    @FXML
    void clickAbout(ActionEvent event) {
        System.out.println(event.getEventType().getName());
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setWidth(800);
        alert.setTitle("About");
        Image image = new Image("image/Cyber_Eye-512.png");
        ImageView imageView = new ImageView(image);
        imageView.setFitHeight(100);
        imageView.setFitWidth(100);
        alert.setGraphic(imageView);
        alert.setHeaderText("");
        alert.setContentText("Autorem aplikacji jest \nMonsieur Mateusz Gałecki von Gałkensztajn.\n" +
                "Kontakt: \n@: mateusz.galecki@outlook.com\n" +
                "Pozdro 600 i do zobaczenia na szlaku :)");
        alert.showAndWait();

    }
    @FXML
    void initialize() {
        scanAndSaveButton.setDisable(true);
        removePathButton.setDisable(true);
        Settings.loadSettings();
        listOfPaths.setItems(fillListViewWithPaths());
        if(!listOfPaths.getItems().isEmpty()){
            scanAndSaveButton.setDisable(false);
            removePathButton.setDisable(false);
        }
        assert addPathButton != null : "fx:id=\"addPathButton\" was not injected: check your FXML file 'sample.fxml'.";
        assert removePathButton != null : "fx:id=\"removePathButton\" was not injected: check your FXML file 'sample.fxml'.";
        assert scanAndSaveButton != null : "fx:id=\"scanAndSaveButton\" was not injected: check your FXML file 'sample.fxml'.";

    }

    private ObservableList<String> fillListViewWithPaths() {
        ArrayList<String> stringArrayList = new ArrayList<>();
        ArrayList<File> listOfScannedPaths = Settings.getListOfScannedPaths();

        for (File file : listOfScannedPaths) {
            stringArrayList.add(file.getAbsolutePath());
        }
        ObservableList<String> listOfPaths = FXCollections.<String>observableArrayList(stringArrayList);
        return listOfPaths;

    }



}
