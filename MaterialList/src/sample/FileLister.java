package sample;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.concurrent.Task;
import javafx.scene.control.ProgressBar;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

public class FileLister extends Task<Void> {

    public FileLister() {
    }

    @Override
    protected Void call() throws Exception {
        PrintWriter writer = null;

        try {
            writer = new PrintWriter(new File(Settings.getOutputScannedPath()), "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        for (File directory : Settings.getListOfScannedPaths()) {

            double count = 0;
            double files = directory.listFiles().length;
            updateMessage(directory.getAbsolutePath());


            for (File file : directory.listFiles()) {
                Counter.setFilesCount(Counter.getFilesCount() + 1);
                count++;
                if (file.isFile()) {

                    writer.println(file.getName());

                }
                updateProgress(count, files);
            }
            Counter.setFilesCount(0);

        }

        writer.close();
        return null;
    }


}
